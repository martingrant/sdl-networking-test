//
//  Source.cpp
//	SDLClient
//
//  Created by Martin Grant on 13/02/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#include "Client.h"

int main(int argc, char * argv[])
{
	Client client("localhost", 1234, 512);

  client.init();
  client.connectToServer();

	while (client.update())
		continue;

	return 0;
}
