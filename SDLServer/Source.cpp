//
//  Source.cpp
//	SDLServer
//
//  Created by Martin Grant on 13/02/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#include "Server.h"

int main(int argc, char * argv[])
{
	Server server(1234, 512, 4);	// port, buffer size, max sockets

  //server.init();

	while (server.update())
		continue;

	return 0;
}