#pragma once

#include <iostream>
#include <string>
#include <sstream>
#include <conio.h>	// non-blocking input, _khbit, _getche etc.
#include <thread>

#ifdef _WIN32
#include <SDL_net.h>
#elif __APPLE__
#include <SDL2_net/SDL_net.h>
#endif

class Client
{
public:
  Client(std::string host, unsigned int port, unsigned int bufferSize);
  ~Client(void);

  void init();
  bool connectToServer();
  bool update();
  void receiveActivity();
  void sendActivity();

  void send(std::string message);

private:
  bool m_shutDownClient;

  unsigned int m_serverPort;
  unsigned int m_bufferSize;
  unsigned int m_timeOut;

  std::string m_userName;

  std::string m_serverHost;
  IPaddress m_serverIP;
  TCPsocket m_serverSocket;
  TCPsocket m_clientSocket;
  SDLNet_SocketSet m_socketSet;

  char* m_buffer;
  std::string m_userInput;
  int m_inputLength;

  const char* m_messageCodes[10];


private:
  bool resolveHost();

  std::string convertIP(IPaddress address);
  std::string convertIP(TCPsocket socket);

  std::thread m_receiveThread;
  bool m_connected;

  void send(TCPsocket socket, std::string message);

};

